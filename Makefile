#!/usr/bin/env make

PYTHON = py
.PHONY: pydoc

all:

venv:
	[ -d .venv ] || $(PYTHON) -m venv .venv
	@echo "Now activate the Python virtual environment:\n. .venv/bin/activate"
	@echo "Type 'deactivate' to deactivate."

install:
	$(PYTHON) -m pip install -r requirements.txt

installed:
	$(PYTHON) -m pip list

clean:
	rm -f .coverage *.pyc
	rm -rf __pycache__
	rm -rf htmlcov

clean-doc:
	rm -rf doc

clean-all: clean clean-doc
	rm -rf .venv

coverage:
	coverage run -m unittest discover . "*_test.py"
	coverage html
	coverage report -m

pylint:
	pylint *.py

flake8:
	flake8

pdoc:
	rm -rf doc/api
	rm -rf doc/uml
	pdoc --html -o doc/api .

doc: pdoc pyreverse

pyreverse:
	install -d doc/uml
	cp index.html doc/uml
	pyreverse $(PWD)
	dot -Tpng classes.dot -o doc/uml/class_diagram.png
	dot -Tpng packages.dot -o doc/uml/packages_war.png
	rm -f classes.dot packages.dot
	ls -l doc/uml

radon-cc:
	radon cc . -a

radon-mi:
	radon mi .

radon-raw:
	radon raw .

radon-hal:
	radon hal .

bandit:
	bandit -r .

lint: flake8 pylint

test: lint coverage

run:
	$(PYTHON) main.py

