"""Deck class file."""
import random


class Deck:
    """Handles a full deck of cards."""

    control_deck_length = 52
    deck_suits = 4
    deck_values = 13
    suit_list = ["Clubs", "Hearts", "Spades", "Diamonds"]

    def __init__(self):
        """Deck constructor."""
        self.made_deck = []

    def generate(self):
        """Generate the cards in the deck."""
        self.made_deck = []
        for i in self.suit_list:
            for j in range(self.deck_values):
                self.made_deck.append(Card((j + 1, i)))
        return self.made_deck

    def shuffle_deck(self):
        """Shuffles deck."""
        random.shuffle(self.made_deck)

    def deal_cards(self, difficulty):
        """Deals cards into two halves."""
        middle_index = len(self.made_deck) / 2

        first_half = self.made_deck[:int(middle_index) - difficulty * 3]
        second_half = self.made_deck[int(middle_index) - difficulty * 3:]
        return first_half, second_half

    def get_card_list(self):
        """Return tuples for each card in the deck."""
        tuple_list = []
        for i in self.made_deck:
            tuple_list.append(i.get_card_tuple())
        return tuple_list


class Card:  # pylint: disable=R0903
    """Card class that stores number and suit in a tuple."""

    def __init__(self, pair):
        """Card constructor sets pair using given arg."""
        self.card_pair = pair

    def get_card_tuple(self):
        """Return the tuple containing number and suit."""
        return self.card_pair
