"""Player class file."""


def create_id(last_id):
    """Return passed argument plus 1."""
    return last_id+1


class Player:  # pylint: disable=R0903
    """Saves name and id for one player."""

    def __init__(self, name, last_id):
        """Assign name to the passed string.

        uses create_id() to assign a unique id.
        """
        self.name = name
        self.id_var = create_id(last_id)
