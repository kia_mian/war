"""CardHand class file."""


class CardHand:
    """Object that stores the current hand of a given player."""

    def __init__(self, hand):
        """Card hand constructor."""
        self.hand = hand

    def get_next_card_hand(self):
        """Get and remove next card from hand."""
        next_card = self.hand[0]
        self.hand.remove(self.hand[0])
        return next_card

    def add_cards_to_hand(self, add_card):
        """Add cards to hand."""
        for i in add_card:
            self.hand.append(i)

    def get_war_hand(self):
        """Get 3 cards for war."""
        card_list = []

        if len(self.hand) > 4:
            for i in range(3):
                i += 0
                card_list.append(self.get_next_card_hand())
        else:
            for i in range(len(self.hand) - 1):
                card_list.append(self.get_next_card_hand())
        return card_list

    def is_hand_empty(self):
        """Check if hand is empty."""
        return bool(len(self.hand) == 0)
