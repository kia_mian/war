"""Histogram class file."""
from src import loader  # pylint: disable=E0401


def load_histogram(filename):
    """Load histogram from the passed filename."""
    try:
        return loader.reader(filename)
    except FileNotFoundError:
        list1 = []
        loader.writer(list1, filename)
        return loader.reader(filename)


class Histogram:
    """Stores the winner and loser for the last 10 games."""

    def __init__(self):
        """Load the currently saved histogram from histogram.p."""
        self.histogram_list = load_histogram("histogram.p")

    def add_entry(self, entry):
        """Add the passed string to the histogram.

        Ensures that the
        length is at max 10.
        """
        if len(self.histogram_list) > 9:
            self.histogram_list.remove(self.histogram_list[0])
        self.histogram_list.append(entry)

    def save_file(self, histogram_path):
        """Save saves currently loaded histogram.

        Into the passed filename.
        """
        loader.writer(self.histogram_list, histogram_path)
