"""Highscore class file."""

from src import loader  # pylint: disable=E0401


def load_high_score_file(filename):
    """Load list of currently saved highscores.

    from the passed filename.
    """
    try:
        return loader.reader(filename)
    except FileNotFoundError:
        dict1 = {}
        loader.writer(dict1, filename)
        return loader.reader(filename)


class HighScore:
    """Class that handles storing the highscores of each player."""

    def __init__(self, player_list):
        """Highscore constructor.

        Loads from the file high_score_list.p and assigns
        passed list to player_list variable.
        """
        self.high_score_list = load_high_score_file("high_score_list.p")
        self.player_list = player_list

    def get_id_list(self):
        """Get list of player ids currently registered with a highscore."""
        return self.high_score_list.keys()

    def save_high_score_file(self, highscore_path):
        """Save list of currently loaded highscores to the passed filename."""
        loader.writer(self.high_score_list, highscore_path)

    def add_highscore(self, id_var):
        """Add highscore.

        Either by adding 1 to the number of wins
        achieved by an already registered player or by adding the
        player id and setting their highscore to 1.
        """
        if id_var not in self.high_score_list.keys():
            self.high_score_list[id_var] = 1
        else:
            self.high_score_list[id_var] += 1
