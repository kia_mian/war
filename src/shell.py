"""Shell class file."""

import cmd
import random
from playsound import playsound
from src import game  # pylint: disable=E0401


def thats_illegal():
    """Rund sound for illegal parameters given."""
    sound_chooser = random.randrange(2)
    if sound_chooser == 1:
        playsound("resources/illegal.mp3")
    else:
        playsound("resources/bitconnect.mp3")


class Shell(cmd.Cmd):
    """Shell for game."""

    intro = (
        "Welcome to the game. Type help or ? at any time to see commands.\n"
    )
    prompt = "(game) "

    def __init__(self):
        """Create game object."""
        super().__init__()
        self.game = game.Game()

    def do_add_player(self, name):
        """Create a new player.

        --------------------

        add_player <name>
        """
        if not self.game.gamestate:
            if not name:
                thats_illegal()
                print("Add name after add_player")
            else:
                self.game.add_player(name)
                self.game.save_player_list(self.game.player_path)
                print(f"Successfully added {name}")
        else:
            thats_illegal()
            print("Not allowed while the game is running")

    def do_view_players(self, _):
        """View the list of current players and their ids."""
        if not self.game.gamestate:
            if len(self.game.player_list) == 1:
                print("No players registered")
            else:
                print("\n{:<3}".format("id") + " : " + "{:>10}".format("name"))
                print("_________________\n")
                for i in self.game.player_list:
                    if i.id_var != -1:
                        print(f"{i.id_var :<3} : {i.name :>10}")
            print()
        else:
            thats_illegal()
            print("Not allowed while the game is running")

    def do_difficulty(self, arg):
        """Set the difficulty of the AI player.

        Normal (0): each player gets 26 cards
        Hard   (1): AI gets 29 cards and player gets 23
        Master (2): AI gets 32 cards and player gets 20.
        ------------------------------------------------
        difficutly <difficulty>
        """
        if not self.game.gamestate:
            if not arg:
                thats_illegal()
                print("Please use difficulty followed by 0, 1, or 2" +
                      " for normal, hard, or master")
                return

            args = arg.split()

            if len(args) > 1:
                thats_illegal()
                print("Please only enter one argument")
                return

            try:
                if int(args[0]) == 0:
                    self.game.set_difficulty(0)
                elif int(args[0]) == 1:
                    self.game.set_difficulty(1)
                elif int(args[0]) == 2:
                    self.game.set_difficulty(2)
                else:
                    thats_illegal()
                    print("Please enter 0, 1, or 2")
            except ValueError:
                thats_illegal()
                print("Please only enter 0, 1, or 2")
        else:
            thats_illegal()
            print("Not allowed while the game is running")

    def do_change_player_name(self, arg):
        """Change Player Name.

        -----------------------------------------
        change_player_name <id> <new_name>
        """
        if not self.game.gamestate:
            args = arg.split(" ")

            if len(args) == 2:
                msg = self.game.change_player_name(args[0], args[1])
                print(msg)
            else:
                thats_illegal()
                print("Enter id and name you want to change to")
        else:
            thats_illegal()
            print("Not allowed while the game is running")

    def do_view_highscore(self, _):
        """Show the number of wins for each player."""
        if not self.game.gamestate:
            if len(self.game.high_score_list.high_score_list) == 0:
                print("No highscores\n")
            else:
                print(
                    "\n{:<10} {:<15} {:<10}".format(
                        "Player Id", "Player Name", "Win Score"
                    )
                )

                for i in self.game.player_list:
                    for key, value in (
                            self.game.high_score_list.high_score_list.items()
                    ):
                        num = value
                        if i.id_var == key:
                            label = i.name
                            print("{:<10} {:<15} {:<10}".format(key, label, num
                                                                )
                                  )
                        else:
                            continue
            print()
        else:
            thats_illegal()
            print("Not allowed while the game is running")

    def do_view_histogram(self, _):
        """View the outcome of the last 10 games played."""
        if not self.game.gamestate:
            counter = 1
            print()
            for i in reversed(self.game.histogram.histogram_list):
                print(f"{counter :<2}", ": ", i)
                counter += 1
            print()
        else:
            thats_illegal()
            print("Not allowed while the game is running")

    def do_exit(self, _):
        """Exit the game and saves all information."""
        print("lmao see ya")
        self.game.save_game_session()
        return True

    def do_start(self, arg1):
        """Start the game.

        Providing one player ID
        starts a game against the AI, providing 2
        player IDs starts a game between the two players.
        -------------------------------------------------
        start <player1_id> (<player2_id>)
        """
        if not self.game.gamestate:
            args = arg1.split(" ")

            if not arg1:
                thats_illegal()
                print("No less than 1 argument after 'start'")

            elif len(args) == 1:
                start_msg = self.game.start(args[0], -1)
                print(start_msg)
                if self.game.gamestate:
                    playsound("resources/duel.mp3")
            elif len(args) == 2:
                start_msg = self.game.start(args[0], args[1])
                print(start_msg)
                if self.game.gamestate:
                    playsound("resources/duel.mp3")
            else:
                thats_illegal()
                print("No more than 2 arguments after 'start'")
        else:
            thats_illegal()
            print("Not allowed while the game is running")

    def do_flip(self, _):
        """Flip the next two cards in each player's hand."""
        if self.game.gamestate:
            drawn_cards, win = self.game.game_logic()
            print(drawn_cards)
            print(win)
            print("----")
        else:
            thats_illegal()
            print("Not allowed while the game is not running")

    def do_give_up(self, arg):
        """Surrender the current game.

        ---------------------------
        give_up <id>
        """
        if self.game.gamestate:
            if self.game.ai_game:
                self.game.give_up(self.game.player1.id_var)
            elif not arg:
                thats_illegal()
                print(
                    "Enter either 1 or 2 for" +
                    " the player that wishes to surrender"
                )
            elif arg.strip() == "1":
                self.game.give_up(self.game.player1.id_var)
            elif arg.strip() == "2":
                self.game.give_up(self.game.player2.id_var)
            else:
                thats_illegal()
                print("ERROR")
        else:
            thats_illegal()
            print("Not allowed while the game is not running")

    def do_simulate(self, _):
        """Simulate the current game and get the result."""
        if self.game.gamestate:
            msg = self.game.simulate()
            print(msg)
        else:
            thats_illegal()
            print("Not allowed while the game is not running")

    def do_card_count(self, _):
        """Print the number of cards in each player's hand."""
        if self.game.gamestate:
            print(
                f"\nPlayer 1 has {len(self.game.player1_hand.hand)} cards left"
            )
            print(
                f"\nPlayer 2 has {len(self.game.player2_hand.hand)} cards left"
            )
        else:
            thats_illegal()
            print("Not allowed while the game is not running")
