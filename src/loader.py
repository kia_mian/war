"""File loader and writer class."""
import pickle


def writer(write_list, filename):
    """Write to file using pickle."""
    with open("pickle_files/" + filename, "wb") as f_var:
        pickle.dump(write_list, f_var)
        return write_list, f_var


def reader(filename):
    """Read from pickle file."""
    try:
        with open("pickle_files/" + filename, "rb") as f_var:
            return pickle.load(f_var)
    except FileNotFoundError as error:
        raise FileNotFoundError from error
