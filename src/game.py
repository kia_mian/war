"""Game class file."""


from src import loader  # pylint: disable=E0401
from src import high_score  # pylint: disable=E0401
from src import deck  # pylint: disable=E0401
from src import card_hand  # pylint: disable=E0401
from src import histogram  # pylint: disable=E0401
from src import player  # pylint: disable=E0401


def load_player_list(filename):
    """Load player list from the passed filename."""
    try:
        return loader.reader(filename)
    except FileNotFoundError:
        list1 = [player.Player("AI", -2)]
        loader.writer(list1, filename)
        return loader.reader(filename)


def comparator(card1, card2):
    """Compare the two cards passed and returns true if the numbers are equal.

    Otherwise it returns false.
    """
    return bool(card1.card_pair[0] == card2.card_pair[0])


class Game:  # pylint: disable=R0902
    """Main class for handling game functions."""

    def __init__(self):
        """Game constructor. Sets up player, highscore, and histogram."""
        self.player_list = load_player_list("player_list.p")
        self.high_score_list = high_score.HighScore(self.player_list)
        self.histogram = histogram.Histogram()
        self.player1 = None
        self.player2 = None
        self.player1_hand = None
        self.player2_hand = None
        self.card_list = []
        self.gamestate = False
        self.ai_game = False
        self.winner = None
        self.loser = None
        self.draw_counter = 0
        self.histogram_path = "histogram.p"
        self.highscore_path = "high_score_list.p"
        self.player_path = "player_list.p"
        self.war_bool = False
        self.difficulty = 0

    def load_last_id(self):
        """Load last ID assigned to the last player."""
        if len(self.player_list) != 1:
            last_id = self.player_list[len(self.player_list) - 1].id_var
        else:
            last_id = 0
        return last_id

    def save_player_list(self, filename):
        """Save player list to the passed filename."""
        loader.writer(self.player_list, filename)

    def game_logic(self):
        """Get the two next cards from each player.

        Then compares them, runs war if needed, else
        returns the winner of the cards
        and adds them to said player's hand.
        """
        comp_bool = True
        while comp_bool and self.gamestate:
            self.check_draw()
            end_game = self.end_game_determiner()
            if end_game:
                break
            card1 = self.player1_hand.get_next_card_hand()
            card2 = self.player2_hand.get_next_card_hand()
            drawn_cards = (f"{self.player1.name} drew a {card1.card_pair[0]}" +
                           f"\n{self.player2.name} drew a {card2.card_pair[0]}"
                           )
            comp_bool = comparator(card1, card2)
            if comp_bool:
                temp_cardlist = self.war()
                self.card_list += temp_cardlist + [card1, card2]
            elif card1.card_pair[0] > card2.card_pair[0]:
                self.player1_hand.add_cards_to_hand(
                    [card1, card2] + self.card_list
                )
                self.card_list = []
                return drawn_cards, self.player1.name + " won this hand"
            else:
                self.player2_hand.add_cards_to_hand(
                    [card1, card2] + self.card_list
                )
                self.card_list = []
                return drawn_cards, self.player2.name + " won this hand"

    def check_draw(self):
        """Check if the draw counter variable is greater than 250.

        Trigger a draw if this is the case
        """
        if self.draw_counter >= 250:
            self.player1_hand.hand = []
            self.player2_hand.hand = []

    def end_game_determiner(self):
        """Determine if a player has lost.

        (their hand has reached 0 cards) or if there is a draw (both
        hands have reached 0).
        """
        msg_var = ""
        if len(self.player2_hand.hand) < 1 and len(self.player1_hand.hand) < 1:
            self.winner = None
            self.loser = None
            self.gamestate = False
            self.add_histogram_entry()
            self.save_game_session()
            msg_var = "Draw"
        elif len(self.player1_hand.hand) == 0:
            self.winner = self.player2
            self.loser = self.player1
            self.gamestate = False
            self.add_histogram_entry()
            self.save_game_session()
            msg_var = "Player 1 lost"
        elif len(self.player2_hand.hand) == 0:
            self.winner = self.player1
            self.loser = self.player2
            self.gamestate = False
            self.add_histogram_entry()
            self.save_game_session()
            msg_var = "Player 2 lost"
        return msg_var

    def draw_counter_add(self):
        """Ensure that if the deck is not shuffled incorrectly.

        If this happens draw.
        """
        if (
            (len(self.player1_hand.hand) == 25
                and len(self.player2_hand.hand) == 27
             )
            or (
                len(self.player1_hand.hand) == 27
                and len(self.player2_hand.hand) == 25
            )
            or (
                len(self.player1_hand.hand) == len(self.player2_hand.hand)
            )
        ):
            self.draw_counter += 1

    def war(self):
        """Grab 3 cards from each player's hand.

        Add all the cards into on list that is returned.
        """
        war_hand_1 = self.player1_hand.get_war_hand()
        war_hand_2 = self.player2_hand.get_war_hand()
        merged_hands = war_hand_1 + war_hand_2
        return merged_hands

    def start(self, player1id, player2id):
        """Start the game.

        Finds the entered players by id
        and returns error if the ids do not exist. Also
        initiates gamestate variable and generates and deals
        cards to the players.
        """
        player_found1 = False
        player_found2 = False
        for i in self.player_list:
            if i.id_var == int(player1id):
                self.player1 = i
                player_found1 = True
            elif i.id_var == int(player2id):
                self.player2 = i
                player_found2 = True

        if not player_found1 or not player_found2:
            return "Player ID not found"

        if self.player2.id_var == -1:
            self.ai_game = True

        self.gamestate = True
        deck_start = deck.Deck()
        deck_start.generate()
        deck_start.shuffle_deck()
        if self.ai_game:
            deal1, deal2 = deck_start.deal_cards(self.difficulty)
        else:
            deal1, deal2 = deck_start.deal_cards(0)
        self.player1_hand = card_hand.CardHand(deal1)
        self.player2_hand = card_hand.CardHand(deal2)
        self.draw_counter = 0
        self.card_list = []

        return (
            f"\nPlayer 1: {self.player1.name}\nPlayer 2: {self.player2.name}"
        )

    def add_histogram_entry(self):
        """Add entry regarding results of game to histogram."""
        if self.winner is None:
            self.histogram.add_entry(
                f"{self.player1.name} drew against {self.player2.name}")
        else:
            self.histogram.add_entry(
                f"{self.winner.name} won against {self.loser.name}")
            self.high_score_list.add_highscore(self.winner.id_var)

    def save_game_session(self):
        """Save histogram highscore, and players to their respective paths.

        Specified by class variables.
        """
        self.histogram.save_file(self.histogram_path)
        histogram.load_histogram(self.histogram_path)
        self.high_score_list.save_high_score_file(self.highscore_path)
        high_score.load_high_score_file(self.highscore_path)
        self.save_player_list(self.player_path)

    def give_up(self, player1):
        """Empty the hand length of the player that gives up."""
        if player1 == 1:
            self.player1_hand.hand = []
        elif player1 == 2:
            self.player2_hand.hand = []

        self.end_game_determiner()

    def add_player(self, name):
        """Add player object with name and id to the list of players."""
        self.player_list.append(player.Player(name, self.load_last_id()))

    def change_player_name(self, id_var, name):
        """Change the name of the player.

        with the given ID to the given name.
        """
        msg = "Error. Invalid id"
        player_found = False

        try:
            if int(id_var) < 1:
                return msg

            for i in self.player_list:
                if i.id_var == int(id_var):
                    i.name = name
                    player_found = True

        except ValueError:
            return msg

        if player_found:
            msg = f"Successfuly changed name of {id_var} to {name}"
        return msg

    def set_difficulty(self, difficulty):
        """Set the difficulty to the number passed."""
        self.difficulty = difficulty

    def simulate(self):
        """Simulate and finish an active game.

        By looping game_logic method. Returns the winner and loser at the end.
        """
        while self.gamestate:
            self.game_logic()
        return (f"{self.winner.name} won this game" +
                f" \n{self.loser.name} lost this game")
