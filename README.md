                                                       
    +---------------------------------------------+    
    |+-------------------------------------------+|    
    ||                                           ||    
    ||    E      E      E     E      EEEEEEE     ||    
    ||    E     E E     E    E E     E      E    ||    
    ||     E    E E    E     E E     E       E   ||    
    ||     E    E E    E     E E     E       E   ||    
    ||     E   E   E   E    E   E    E       E   ||    
    ||     E   E   E   E    E   E    E      E    ||    
    ||      E  E   E  E     E   E    EEEEEEE     ||    
    ||      E E     E E    EEEEEEE   E    E      ||    
    ||      E E     E E    E     E   E     E     ||    
    ||      E E     E E    E     E   E     E     ||    
    ||       E       E    E       E  E      E    ||    
    ||       E       E    E       E  E       E   ||    
    ||                                           ||    
    |+-------------------------------------------+|    
    +---------------------------------------------+    
                                                       

=======================================================

-------------------------------------------------------

Intro:
The game war is about dealing a deck of cards evenly into 2 decks, each player holding one half of the deck each,
each player pulls the top card from their own deck and compares it to the enemies pulled card. If the cards are of equal value,
a war starts, you then proceed to pull 3 face down cards and 1 face up card and you compare the face up card against the enemies face up card.
Whoever has the highest valued card wins all pulled cards from that hand, the "war" continues if the face up cards are equal.



-------------------------------------------------------
Pre-requisites:
- Have Python installed
- Have the PC volume at 50%+

-------------------------------------------------------

How to install game pre-requisites:

Linux:
- Open cmd
- Navigate to game root folder
- Open make file and change PYTHON global variable to your Python executable command (example PYTHON=py)
- Run cmd "make venv"
- Activate venv with ". ./venv/bin/activate"
- Run cmd "make install"


Windows:
- Navigate to https://chocolatey.org/install and install Chocolatey
- Run cmd in elevated mode
- Run cmd "choco install make"
- Navigate to game root folder
- Open make file and change PYTHON global variable to your Python executable command (example PYTHON=py)
- Run cmd "make venv"
- Activate venv with ". ./venv/Scripts/activate"
- Run cmd "make install"

-------------------------------------------------------

How to run flake8 and pylint:

- Open git bash
- Navigate to root game folder
- Activate venv enviornment
- Navigate to "war/src" and "war/test" and run "make lint"


-------------------------------------------------------

To generate documentation:

- Open git bash
- Navigate to root game folder
- Activate venv environment
- Run "make doc" 
- Documentation will be generated under "doc" folder "doc/uml" for class and package diagram, "doc/api" for source code documentation


-------------------------------------------------------

To run the game, 
- Open cmd
- Navigate to game root folder
- Run cmd "Make run"

-------------------------------------------------------

Commands : 

- add_player
	Adds another player to the list of players.

- card_count
	Shows the current amount of cards.

- change_player_name
	Change the current players name.

- difficulty
	Adjust the difficulty of the AI.

- exit
	Quit the game.

- flip
	Reveals the next card.

- give_up
	Forfeit the current game.

- help
	See all commands.

- simulate
	Simulates the rest of the current game.

- start 
	Starts the game.

- view_histogram
	Displays the histogram (10 last games).

- view_highscore
	Displays the highshore.

- view_players
	Displays all players.

=======================================================

ENJOY!