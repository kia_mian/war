"""Histogram unittest file."""

import unittest
import os
from src import histogram  # pylint: disable=E0401


class TestHistogramClass(unittest.TestCase):
    """Testing histogram class."""

    def test_create_histogram(self):
        """Test object creation."""
        histogram1 = histogram.Histogram()
        res = histogram1
        self.assertIsInstance(res, histogram.Histogram)

    def test_load_histogram(self):
        """Load histogram."""
        res = len(histogram.load_histogram("histogram_test_file.p"))
        exp = 1
        self.assertEqual(res, exp)
        os.remove("pickle_files/histogram_test_file.p")

    def test_add_entry(self):
        """Save histogram."""
        histogram1 = histogram.Histogram()
        for i in range(11):
            histogram1.add_entry(i)
        res = len(histogram1.histogram_list)
        exp = 10
        self.assertEqual(res, exp)

    def test_save_file(self):
        """Test saving file."""
        histogram1 = histogram.Histogram()
        histogram1.save_file("histogram_test_file.p")
        res1 = histogram.load_histogram("histogram_test_file.p")
        self.assertIsInstance(res1, list)
        os.remove("pickle_files/histogram_test_file.p")
        res2 = histogram.load_histogram("histogram_test_file.p")
        self.assertIsInstance(res2, list)
