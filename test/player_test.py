"""Player unittest file."""

import unittest
from src import player  # pylint: disable=E0401


class TestPlayerClass(unittest.TestCase):
    """Test player class."""

    def test_create_player(self):
        """Testing player creation."""
        res = player.Player("name", 0)
        self.assertIsInstance(res, player.Player)
        self.assertIsNotNone(res.name)

    def test_id_creation(self):
        """Test the creation of an ID."""
        player1 = player.Player("name1", 1)
        player2 = player.Player("name2", 2)

        self.assertEqual(player1.id_var, player2.id_var-1)
