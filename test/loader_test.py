"""File loader unittest file."""
import unittest
import os
from src import loader  # pylint: disable=E0401


class TestLoaderClass(unittest.TestCase):
    """Test loader class."""

    def test_read_from_and_save_to_pickle(self):
        """Testing loading and saving from pickled file."""
        random_list = []

        loader.writer(random_list, "test_list_file.p")
        res1 = loader.reader("test_list_file.p")
        self.assertIsInstance(res1, list)
        os.remove("pickle_files/test_list_file.p")

    def test_read_from_and_save_to_pickle_fail(self):
        """Testing failed read from pickle file."""
        random_list = []

        loader.writer(random_list, "test_ist_file.p")
        self.assertRaises(
            FileNotFoundError, lambda: loader.reader("test_list_file.p")
        )
