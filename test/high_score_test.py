"""Highsore unittest file."""


import unittest
import os
from src import high_score  # pylint: disable=E0401


class TestHighScoreClass(unittest.TestCase):
    """Testing highscore class."""

    def test_get_id_list(self):
        """Gets list of IDs currently registered with a highscore."""
        high_score1 = high_score.HighScore([])
        high_score1.high_score_list = high_score.load_high_score_file(
            "high_score_test_list.p"
        )

        res = high_score1.get_id_list()
        exp = len(res) == len(high_score1.high_score_list.keys())
        self.assertTrue(exp)

    def test_read_write_to_pickle(self):
        """Testing failed read from pickle file."""
        high_score1 = high_score.HighScore([])
        high_score1.save_high_score_file("high_score_test_list.p")
        res1 = high_score.load_high_score_file("high_score_test_list.p")
        self.assertIsInstance(res1, dict)
        os.remove("pickle_files/high_score_test_list.p")
        res2 = high_score.load_high_score_file("high_score_test_list.p")
        self.assertIsInstance(res2, dict)

    def test_add_highscore(self):
        """Test adding new highscore."""
        high_score1 = high_score.HighScore([])
        high_score.load_high_score_file("high_score_test_list.p")
        high_score1.add_highscore(123)
        res = high_score1.high_score_list[123]
        exp = 1
        self.assertEqual(res, exp)
        high_score1.add_highscore(123)
        res1 = high_score1.high_score_list[123]
        exp1 = 2
        self.assertEqual(res1, exp1)
