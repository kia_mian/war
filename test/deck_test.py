"""Deck unittest file."""

import unittest  # pylint: disable=E0401
from src import deck  # pylint: disable=E0401


class TestCardClass(unittest.TestCase):
    """Class decleration."""

    def test_create_card(self):
        """Test create card object."""
        res = deck.Card((int, str))
        self.assertIsInstance(res, deck.Card)
        exp = res.card_pair[0] is not None and res.card_pair[1] is not None
        self.assertTrue(exp)

    def test_get_card_tuple(self):
        """Test card structure."""
        card1 = deck.Card((10, "Hearts"))
        res = card1.get_card_tuple()
        exp = res == card1.card_pair
        self.assertTrue(exp)


class TestDeckClass(unittest.TestCase):
    """Tests Deck class."""

    control_deck = [
        (1, "Clubs"),
        (2, "Clubs"),
        (3, "Clubs"),
        (4, "Clubs"),
        (5, "Clubs"),
        (6, "Clubs"),
        (7, "Clubs"),
        (8, "Clubs"),
        (9, "Clubs"),
        (10, "Clubs"),
        (11, "Clubs"),
        (12, "Clubs"),
        (13, "Clubs"),
        (1, "Hearts"),
        (2, "Hearts"),
        (3, "Hearts"),
        (4, "Hearts"),
        (5, "Hearts"),
        (6, "Hearts"),
        (7, "Hearts"),
        (8, "Hearts"),
        (9, "Hearts"),
        (10, "Hearts"),
        (11, "Hearts"),
        (12, "Hearts"),
        (13, "Hearts"),
        (1, "Spades"),
        (2, "Spades"),
        (3, "Spades"),
        (4, "Spades"),
        (5, "Spades"),
        (6, "Spades"),
        (7, "Spades"),
        (8, "Spades"),
        (9, "Spades"),
        (10, "Spades"),
        (11, "Spades"),
        (12, "Spades"),
        (13, "Spades"),
        (1, "Diamonds"),
        (2, "Diamonds"),
        (3, "Diamonds"),
        (4, "Diamonds"),
        (5, "Diamonds"),
        (6, "Diamonds"),
        (7, "Diamonds"),
        (8, "Diamonds"),
        (9, "Diamonds"),
        (10, "Diamonds"),
        (11, "Diamonds"),
        (12, "Diamonds"),
        (13, "Diamonds"),
    ]

    def test_create_deck(self):
        """Tests creating deck function."""
        res = deck.Deck()
        self.assertIsInstance(res, deck.Deck)

    def test_deck_generate_length(self):
        """Testing generation of entire deck."""
        deck_object = deck.Deck()
        res = len(deck_object.generate())
        exp = deck_object.control_deck_length
        self.assertEqual(res, exp)

    def test_deck_integrity(self):
        """Test deck integrity."""
        deck1 = deck.Deck()
        res = deck1.generate()
        res = deck1.get_card_list()
        exp = self.control_deck
        self.assertEqual(res, exp)

    def test_shuffle_deck(self):
        """Test shuffle deck function."""
        deck1 = deck.Deck()
        preshuffle = list(deck1.generate())
        deck1.shuffle_deck()
        self.assertNotEqual(preshuffle, deck1.made_deck)

    def test_deal_cards(self):
        """Testing dealing cards function / integrity."""
        deck1 = deck.Deck()
        deck1.generate()
        res1, res2 = deck1.deal_cards(0)
        half_deck = deck1.control_deck_length / 2
        exp = (len(res1) == half_deck) and (len(res2) == half_deck)
        self.assertTrue(exp)

    def test_get_card_list(self):
        """Testing getting 1 players deck list."""
        deck1 = deck.Deck()
        deck1.generate()
        res = deck1.get_card_list()
        exp = self.control_deck
        self.assertEqual(res, exp)
