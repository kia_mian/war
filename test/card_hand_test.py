"""CardHand unittest file."""


import unittest
from src import card_hand  # pylint: disable=E0401
from src import deck  # pylint: disable=E0401


class TestCardHandClass(unittest.TestCase):
    """Tests Card_Hand class."""

    def test_create_card_hand(self):
        """Test object creation."""
        card_hand_test = card_hand.CardHand([])
        res = card_hand_test
        self.assertIsInstance(res, card_hand.CardHand)

    def test_get_next_card_hand(self):
        """Test get next card."""
        deck_ini = deck.Deck()
        deck_ini.generate()
        c_hand1 = deck_ini.deal_cards(0)
        card_hand1 = card_hand.CardHand(c_hand1[0])
        exp = card_hand1.hand[0]
        res = card_hand1.get_next_card_hand()
        self.assertEqual(res, exp)
        self.assertNotEqual(exp, c_hand1[0])

    def test_add_cards_to_hand(self):
        """Test add cards to hand."""
        deck_ini = deck.Deck()
        deck_ini.generate()
        c_hand1 = deck_ini.deal_cards(0)
        card_hand1 = card_hand.CardHand(c_hand1[0])
        res = len(card_hand1.hand)
        card_list = [deck.Card((1, "Diamonds"))]
        card_hand1.add_cards_to_hand(card_list)
        exp = len(card_hand1.hand) - len(card_list)
        self.assertEqual(res, exp)

    def test_get_war_hand(self):
        """Test get 4 cards for war."""
        deck_ini = deck.Deck()
        deck_ini.generate()
        c_hand1 = deck_ini.deal_cards(0)
        current_hand = card_hand.CardHand(c_hand1[0])
        exp = 3
        res = len(current_hand.get_war_hand())
        self.assertEqual(exp, res)
        current_hand.hand = current_hand.hand[0:3]
        exp2 = 2
        res2 = len(current_hand.get_war_hand())
        self.assertEqual(exp2, res2)
