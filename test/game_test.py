"""Game unittest file."""

import unittest
import os
from src import game  # pylint: disable=E0401
from src import player  # pylint: disable=E0401
from src import card_hand  # pylint: disable=E0401
from src import deck  # pylint: disable=E0401


class TestGameClass(unittest.TestCase):
    """Testing game class."""

    def test_create_game_object(self):
        """Object creation testing."""
        game_object = game.Game()
        self.assertIsInstance(game_object, game.Game)

    def test_last_id_returned(self):
        """Testing getting correct last id."""
        game1 = game.Game()
        game1.player_list = [player.Player("name", -1)]
        res = game1.load_last_id()
        exp = 0
        self.assertEqual(res, exp)
        game1.player_list = [
            player.Player("name", -1),
            player.Player("name", 1),
        ]
        res2 = game1.load_last_id()
        exp2 = 2
        self.assertEqual(res2, exp2)

    def test_read_write_to_pickle(self):
        """Testing failed read from pickle file."""
        game_object = game.Game()
        game_object.player_path = "test_file_player.p"
        game_object.histogram_path = "histogram_test_file.p"
        game_object.highscore_path = "high_score_test_file.p"
        game_object.save_player_list(game_object.player_path)
        res1 = game.load_player_list(game_object.player_path)
        self.assertIsInstance(res1, list)
        os.remove("pickle_files/" + game_object.player_path)
        res2 = game.load_player_list(game_object.player_path)
        self.assertIsInstance(res2, list)

    def test_comparator(self):
        """Testing flipping card."""
        card1 = deck.Card((10, "Hearts"))
        card2 = deck.Card((10, "Clubs"))
        res = game.comparator(card1, card2)
        self.assertTrue(res)
        card1 = deck.Card((11, "Hearts"))
        card2 = deck.Card((2, "Clubs"))
        res = game.comparator(card1, card2)
        self.assertFalse(res)

    def test_is_hand_empty(self):
        """Testing whether hand is empty or not."""
        card_hand1 = card_hand.CardHand([])
        exp = card_hand1.is_hand_empty()
        self.assertTrue(exp)
        card_hand1.hand.append("a")
        exp1 = card_hand1.is_hand_empty()
        self.assertFalse(exp1)

    def test_war(self):
        """Testing that war gets the right number of cards."""
        game_object = game.Game()
        deck_object = deck.Deck()
        deck_object.generate()
        deal1, deal2 = deck_object.deal_cards(game_object.difficulty)
        game_object.player1_hand = card_hand.CardHand(deal1)
        game_object.player2_hand = card_hand.CardHand(deal2)
        res = game_object.war()
        self.assertEqual(len(res), 6)

    def test_game_logic_tie(self):
        """Test the main game logic.

        Correctly gets the cards, check who wins and returns the
        correct winner.
        """
        game_object = game.Game()
        game_object.histogram_path = "histogram_test_file.p"
        game_object.highscore_path = "high_score_test_file.p"
        game_object.player_path = "test_file_player.p"
        deck_object = deck.Deck()
        deck_object.generate()
        deal1, deal2 = deck_object.deal_cards(game_object.difficulty)
        game_object.player1_hand = card_hand.CardHand(deal1)
        game_object.player2_hand = card_hand.CardHand(deal2)
        game_object.game_logic()
        res = len(game_object.card_list)
        exp = 0
        self.assertEqual(res, exp)
        self.assertEqual(game_object.winner, None)
        self.assertEqual(game_object.loser, None)

    def test_game_logic_player_win(self):
        """Testing that each player can correctly win."""
        game_object = game.Game()
        game_object.histogram_path = "histogram_test_file.p"
        game_object.highscore_path = "high_score_test_file.p"
        game_object.player_path = "test_file_player.p"
        game_object.player_list = [
            player.Player("AI", -2),
            player.Player("test", 0),
            player.Player("test", 1),
        ]
        game_object.start(1, 2)
        game_object.player1_hand = card_hand.CardHand(
            [deck.Card((3, "Hearts"))]
        )
        game_object.player2_hand = card_hand.CardHand([])
        exp = game_object.end_game_determiner()
        self.assertEqual(exp, "Player 2 lost")
        self.assertEqual(game_object.winner, game_object.player1)
        self.assertEqual(game_object.loser, game_object.player2)
        game_object.player1_hand = card_hand.CardHand([])
        game_object.player2_hand = card_hand.CardHand(
            [deck.Card((3, "Hearts"))]
        )
        exp = game_object.end_game_determiner()
        self.assertEqual(exp, "Player 1 lost")
        self.assertEqual(game_object.winner, game_object.player2)
        self.assertEqual(game_object.loser, game_object.player1)

    def test_check_draw(self):
        """Testing that the players can draw."""
        game_object = game.Game()
        game_object.player_path = "test_file_player.p"
        game_object.histogram_path = "histogram_test_file.p"
        game_object.highscore_path = "high_score_test_file.p"
        game_object.player_list = [
            player.Player("AI", -2),
            player.Player("test", 0),
            player.Player("test", 1),
        ]
        game_object.start(1, 2)
        deck_object = deck.Deck()
        deck_object.generate()
        deal1, deal2 = deck_object.deal_cards(game_object.difficulty)
        game_object.player1_hand = card_hand.CardHand(deal1)
        game_object.player2_hand = card_hand.CardHand(deal2)
        game_object.gamestate = True
        game_object.draw_counter = 250
        game_object.game_logic()
        self.assertEqual(game_object.player1_hand.hand, [])
        self.assertEqual(game_object.player2_hand.hand, [])

    def test_draw_counter_add(self):
        """Test that the counter.

        Will add to draw given the correct conditions.
        """
        game_object = game.Game()
        deck_object = deck.Deck()
        deck_object.generate()
        deal1, deal2 = deck_object.deal_cards(game_object.difficulty)
        game_object.player1_hand = card_hand.CardHand(deal1)
        game_object.player2_hand = card_hand.CardHand(deal2)
        game_object.draw_counter_add()
        exp = game_object.draw_counter == 1
        self.assertTrue(exp)
        game_object.player1_hand.hand = game_object.player1_hand.hand[:-1]
        game_object.player2_hand.hand.append("test")
        game_object.draw_counter_add()
        exp = game_object.draw_counter == 2
        self.assertTrue(exp)
        game_object.player1_hand.hand += [
            "test1",
            "test2",
        ]
        game_object.player2_hand.hand = game_object.player2_hand.hand[:-2]
        game_object.draw_counter_add()
        exp = game_object.draw_counter == 3
        self.assertTrue(exp)

    def test_start(self):
        """Test the game can correctly start.

        Find the correct players or assign the game as an ai game.
        """
        game_object = game.Game()
        game_object.player_path = "test_file_player.p"
        game_object.histogram_path = "histogram_test_file.p"
        game_object.highscore_path = "high_score_test_file.p"
        game_object.player_list = [
            player.Player("AI", -2),
            player.Player("test", 0),
            player.Player("test", 1),
        ]
        res = game_object.start(1, 2)
        self.assertEqual(res, "\nPlayer 1: test\nPlayer 2: test")
        res1 = game_object.start(3, -1)
        self.assertEqual(res1, "Player ID not found")
        game_object.start(2, -1)
        self.assertTrue(game_object.ai_game)

    def test_add_player(self):
        """Test that a new player.

        Can be created and added to the list of saved players.
        """
        game_object = game.Game()
        res = len(game_object.player_list)
        game_object.add_player("test")
        self.assertEqual(len(game_object.player_list), res + 1)

    def test_simulate(self):
        """Test game simluation.

        Get the right result.
        """
        game_object = game.Game()
        game_object.player_path = "test_file_player.p"
        game_object.histogram_path = "histogram_test_file.p"
        game_object.highscore_path = "high_score_test_file.p"
        game_object.player_list = [
            player.Player("AI", -2),
            player.Player("test", 0),
            player.Player("test", 1),
        ]
        game_object.start(1, -1)
        res = game_object.gamestate
        game_object.simulate()
        self.assertNotEqual(res, game_object.gamestate)

    def test_give_up(self):
        """Testing that a player can surrender during the game."""
        game_object = game.Game()
        game_object.player_path = "test_file_player.p"
        game_object.histogram_path = "histogram_test_file.p"
        game_object.highscore_path = "high_score_test_file.p"
        game_object.player_list = [
            player.Player("AI", -2),
            player.Player("test", 0),
            player.Player("test", 1),
        ]
        game_object.start(1, 2)
        deck_object = deck.Deck()
        deck_object.generate()
        deal1, deal2 = deck_object.deal_cards(game_object.difficulty)
        game_object.player1_hand = card_hand.CardHand(deal1)
        game_object.player2_hand = card_hand.CardHand(deal2)
        game_object.give_up(1)
        game_object.give_up(2)
        self.assertEqual(len(game_object.player1_hand.hand), 0)
        self.assertEqual(len(game_object.player2_hand.hand), 0)

    def test_change_player_name(self):
        """Testing a player changing their name.

        As well as making sure there are
        no errors when entering non-valid information.
        """
        game_object = game.Game()
        game_object.player_list = [
            player.Player("AI", -2),
            player.Player("test", 0),
            player.Player("test", 1),
        ]
        res = game_object.change_player_name(0, "test")
        self.assertEqual(res, "Error. Invalid id")
        res2 = game_object.change_player_name(3, "test")
        self.assertEqual(res2, "Error. Invalid id")
        res3 = game_object.change_player_name("test", "test")
        self.assertEqual(res3, "Error. Invalid id")
        res2 = game_object.change_player_name(2, "test")
        self.assertEqual(res2, "Successfuly changed name of 2 to test")

    def test_set_difficulty(self):
        """Testing the assignment of the difficulty variable."""
        game_object = game.Game()
        game_object.set_difficulty(2)
        self.assertEqual(game_object.difficulty, 2)
