"""\n\n\n\n\nWelcome to War!

This game is all about winning cards from your
opponent and eventually ending up with the whole deck.

The rules are as follows:
1.  Each round has each player put down 1 card.
    The one with the highest number card wins
    and gets both the cards.
2.  If the cards are equal a war occurs. Each player
    puts down 3 cards and then flip a fourth card.
    Whoever has the higher card wins all the 8 cards
    on the table.
3.  If the two cards put down during the war are equal
    another war plays out in the same
    manner.

It is recommended to turn on audio for the best experience.
"""

from src import shell

if __name__ == "__main__":
    print(__doc__)
    shell.Shell().cmdloop()
